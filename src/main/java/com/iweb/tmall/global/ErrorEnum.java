package com.iweb.tmall.global;

import lombok.Getter;

@Getter
public enum ErrorEnum {
    ACCOUNT_ALREADY_EXISTS("A0111","用户名已存在"),
    ACCOUNT_VERIFICATION_FAILD("A0110","用户名校验失败"),
    PASSWORD_VERIFICATION_FAILD("A0120","密码校验失败"),
    SYSTEM_EXECUTION_ERROR("B0001","系统执行出错"),
    USER_IDENTITY_VERIFICATION_FAILS("A0220","用户身份校验失败"),
    DATABASE_SERVICE_ERROR("C0300","数据库服务出错");
    private String code;
    private String message;
    ErrorEnum(String code,String message){
        this.code = code;
        this.message = message;
    }
}