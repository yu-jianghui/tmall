package com.iweb.tmall.global;

import com.iweb.tmall.global.CustomException;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLException;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionAdvice {
    @ExceptionHandler(Exception.class)
    public ResponseData handleException(Exception e){
        log.error(e.getMessage());
        if(e instanceof CustomException){
            CustomException ex = (CustomException)e;
            return new ResponseData(ex.getErrorEnum());
        }else {
            return new ResponseData(ErrorEnum.SYSTEM_EXECUTION_ERROR);
        }
    }
}
