package com.iweb.tmall.global;

import lombok.Data;

@Data
public class ResponseData {
    private String code;
    private String message;
    private Object data;

    public ResponseData(Object data){
        this.code = "00000";// 一切ok的结果码
        this.data = data;
    }

    public ResponseData(ErrorEnum errorEnum){
        this.code = errorEnum.getCode();
        this.message = errorEnum.getMessage();
    }
}