package com.iweb.tmall.global;

import lombok.Data;

@Data
public class CustomException extends RuntimeException {
    private ErrorEnum errorEnum;
    public CustomException(ErrorEnum errorEnum){
        super(errorEnum.getMessage());
        this.errorEnum = errorEnum;
    }
}