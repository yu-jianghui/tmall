package com.iweb.tmall.controller;

import com.iweb.tmall.controller.BaseController;
import com.iweb.tmall.entity.BaseEntity;
import com.iweb.tmall.entity.Comment;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.BaseParam;
import com.iweb.tmall.param.CommentParam;
import com.iweb.tmall.service.CommentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "评论控制器")
@RestController
@RequestMapping("/api/comment")
public class CommentController extends BaseController<CommentParam, Comment>{
    @Autowired
    private CommentService commentService;

    @Override
    public ResponseData list(CommentParam param) {
        return commentService.getList(param);
    }

    @Override
    public ResponseData update(Comment entity) {
        return commentService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return commentService.deleteById(id);
    }

    @Override
    public ResponseData add(Comment entity) {
        return commentService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return commentService.getById(id);
    }
}
