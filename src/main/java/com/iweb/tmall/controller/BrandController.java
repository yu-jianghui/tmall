package com.iweb.tmall.controller;


import com.iweb.tmall.entity.Brand;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.BrandParam;
import com.iweb.tmall.service.BrandService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "品牌控制器")
@RestController
@RequestMapping("/api/brand")
public class BrandController extends BaseController<BrandParam,Brand>{
    @Autowired
    private BrandService brandService;

    @Override
    public ResponseData list(BrandParam param) {
        return brandService.getList(param);
    }

    @Override
    public ResponseData update(Brand entity) {
        return brandService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return brandService.deleteById(id);
    }

    @Override
    public ResponseData add(Brand entity) {
        return brandService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return brandService.getById(id);
    }

    @GetMapping("/valid")
    public ResponseData valid(String name){return brandService.valid(name);}
}
