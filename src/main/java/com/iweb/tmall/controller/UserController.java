package com.iweb.tmall.controller;

import com.iweb.tmall.entity.User;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.UserParam;
import com.iweb.tmall.service.UserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "用户控制器")
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController<UserParam, User>{
    @Autowired
    private UserService userService;
    @Override
    public ResponseData list(UserParam param) {
        return userService.getList(param);
    }

    @Override
    public ResponseData update(User entity) {
        return userService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return userService.deleteById(id);
    }

    @PostMapping("/add")
    public ResponseData add(User entity) {
        return userService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return userService.getById(id);
    }

    @PostMapping("/getOldPassword")
    public ResponseData getOldPassword(String username){return userService.getOldPassword(username);}

    @GetMapping("/validName")
    public ResponseData validUserName(String username){
        return userService.validUserName(username);
    }
}
