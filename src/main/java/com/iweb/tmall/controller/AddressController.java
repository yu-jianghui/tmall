package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Address;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.AddressParam;
import com.iweb.tmall.service.AddressService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "收货地址控制器")
@RestController
@RequestMapping("/api/address")
public class AddressController extends BaseController<AddressParam, Address>{
    @Autowired
    private AddressService addressService;

    @Override
    public ResponseData list(AddressParam param) {
        return addressService.getList(param);
    }

    @Override
    public ResponseData update(Address entity) {
        return addressService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return addressService.deleteById(id);
    }

    @Override
    public ResponseData add(Address entity) {
        return addressService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return addressService.getById(id);
    }
}
