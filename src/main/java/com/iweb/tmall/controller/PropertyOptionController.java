package com.iweb.tmall.controller;

import com.iweb.tmall.entity.PropertyOption;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.PropertyOptionParam;
import com.iweb.tmall.service.PropertyOptionService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品属性选项控制器")
@RestController
@RequestMapping("/api/property/option")
public class PropertyOptionController extends BaseController<PropertyOptionParam, PropertyOption>{
    @Autowired
    private PropertyOptionService propertyOptionService;
    @Override
    public ResponseData list(PropertyOptionParam param) {
        return propertyOptionService.getList(param);
    }

    @Override
    public ResponseData update(PropertyOption entity) {
        return propertyOptionService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return propertyOptionService.deleteById(id);
    }

    @Override
    public ResponseData add(PropertyOption entity) {
        return propertyOptionService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return propertyOptionService.getById(id);
    }
}
