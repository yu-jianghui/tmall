package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Praise;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.PraiseParam;
import com.iweb.tmall.service.PraiseService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "点赞控制器")
@RestController
@RequestMapping("/api/praise")
public class PraiseController extends BaseController<PraiseParam, Praise>{
    @Autowired
    private PraiseService praiseService;
    @Override
    public ResponseData list(PraiseParam param) {
        return praiseService.getList(param);
    }

    @Override
    public ResponseData update(Praise entity) {
        return praiseService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return praiseService.deleteById(id);
    }

    @Override
    public ResponseData add(Praise entity) {
        return praiseService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return praiseService.getById(id);
    }
}
