package com.iweb.tmall.controller;

import com.iweb.tmall.entity.SpecificationOption;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.SpecificationOptionParam;
import com.iweb.tmall.service.SpecificationOptionService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品规格选项控制器")
@RestController
@RequestMapping("/api/specification/option")
public class SpecificationOptionController extends BaseController<SpecificationOptionParam, SpecificationOption>{
    @Autowired
    private SpecificationOptionService specificationOptionService;

    @Override
    public ResponseData list(SpecificationOptionParam param) {
        return specificationOptionService.getList(param);
    }

    @Override
    public ResponseData update(SpecificationOption entity) {
        return specificationOptionService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return specificationOptionService.deleteById(id);
    }

    @Override
    public ResponseData add(SpecificationOption entity) {
        return specificationOptionService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return specificationOptionService.getById(id);
    }
}
