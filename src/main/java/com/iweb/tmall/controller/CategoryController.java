package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Category;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.CategoryParam;
import com.iweb.tmall.service.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "分类控制器")
@RestController
@RequestMapping("/api/category")
public class CategoryController extends BaseController<CategoryParam,Category>{
    @Autowired
    private CategoryService categoryService;

    @Override
    public ResponseData list(CategoryParam param) {
        return categoryService.getList(param);
    }

    @Override
    public ResponseData update(Category entity) {
        return categoryService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return categoryService.deleteById(id);
    }

    @Override
    public ResponseData add(Category entity) {
        return categoryService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return categoryService.getById(id);
    }

    @GetMapping("/valid")
    public ResponseData valid(String name){return categoryService.valid(name);}
}
