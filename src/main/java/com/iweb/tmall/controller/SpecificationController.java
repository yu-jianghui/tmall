package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Specification;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.SpecificationParam;
import com.iweb.tmall.service.SpecificationService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品规格控制器")
@RestController
@RequestMapping("/api/specification")
public class SpecificationController extends BaseController<SpecificationParam, Specification>{
    @Autowired
    private SpecificationService specificationService;
    @Override
    public ResponseData list(SpecificationParam param) {
        return specificationService.getList(param);
    }

    @Override
    public ResponseData update(Specification entity) {
        return specificationService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return specificationService.deleteById(id);
    }

    @Override
    public ResponseData add(Specification entity) {
        return specificationService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return specificationService.getById(id);
    }
}
