package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Sku;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.SkuParam;
import com.iweb.tmall.service.SkuService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品sku控制器")
@RestController
@RequestMapping("/api/sku")
public class SkuController extends BaseController<SkuParam, Sku>{
    @Autowired
    private SkuService skuService;
    @Override
    public ResponseData list(SkuParam param) {
        return skuService.getList(param);
    }

    @Override
    public ResponseData update(Sku entity) {
        return skuService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return skuService.deleteById(id);
    }

    @Override
    public ResponseData add(Sku entity) {
        return skuService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return skuService.getById(id);
    }
}
