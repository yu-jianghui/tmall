package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Spu;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.SpuParam;
import com.iweb.tmall.service.SpuService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品spu控制器")
@RestController
@RequestMapping("/api/spu")
public class SpuController extends BaseController<SpuParam, Spu> {
    @Autowired
    private SpuService spuService;
    @Override
    public ResponseData list(SpuParam param) {
        return spuService.getList(param);
    }

    @Override
    public ResponseData update(Spu entity) {
        return spuService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return spuService.deleteById(id);
    }

    @Override
    public ResponseData add(Spu entity) {
        return spuService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return spuService.getById(id);
    }
}
