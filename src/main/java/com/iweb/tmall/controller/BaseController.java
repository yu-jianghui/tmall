package com.iweb.tmall.controller;

import com.iweb.tmall.entity.BaseEntity;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.BaseParam;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/*
 *  控制器基础父类
 * */
public abstract class BaseController<P extends BaseParam,E extends BaseEntity> {
    /**
     * 分页多条件查询
     * @param param 子类重写后，参数类型是BaseParam的子类即可
     * @return      响应数据对象
     */
    @GetMapping("/list")
    public abstract ResponseData list(P param);

    /**
     * 修改一条数据
     * @param entity    控制器子类重写后，参数类型是BaseEntity的子类即可
     * @return          响应数据对象
     */
    @PostMapping("/update")
    public abstract ResponseData update(E entity);

    /**
     * 删除一条数据(做逻辑删除，不要物理删除)
     * @param id    被删除的数据的主键，只需要将is_delete更新为1即可
     * @return      响应数据对象
     */
    @PostMapping("/delete")
    public abstract ResponseData delete(Integer id);

    /**
     * 插入一条数据
     * @param entity 控制器子类重写后, 参数类型是BaseEntity的子类即可
     * @return 响应数据对象
     */
    @PostMapping("/add")
    public abstract ResponseData add(E entity);

    /**
     * 根据Id查询一条完整的数据
     * @param id 数据的主键Id
     * @return 响应数据对象
     */
    @GetMapping("/view")
    public abstract ResponseData view(Integer id);
}