package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Property;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.PropertyParam;
import com.iweb.tmall.service.PropertyService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "商品属性控制器")
@RestController
@RequestMapping("/api/property")
public class PropertyController extends BaseController<PropertyParam, Property>{
    @Autowired
    private PropertyService propertyService;
    @Override
    public ResponseData list(PropertyParam param) {
        return propertyService.getList(param);
    }

    @Override
    public ResponseData update(Property entity) {
        return propertyService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return propertyService.deleteById(id);
    }

    @Override
    public ResponseData add(Property entity) {
        return propertyService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return propertyService.getById(id);
    }
}
