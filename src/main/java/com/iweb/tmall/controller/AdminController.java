package com.iweb.tmall.controller;

import com.iweb.tmall.entity.Admin;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.AdminParam;
import com.iweb.tmall.service.AdminService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Api(tags = "管理员控制器")
@RestController
@RequestMapping("/api/admin")
public class AdminController extends BaseController<AdminParam,Admin>{

    @Autowired
    private AdminService adminService;

    @Override
    public ResponseData list(AdminParam param) {
        return adminService.getList(param);
    }

    @Override
    public ResponseData update(Admin entity) {
        return adminService.updateById(entity);
    }

    @Override
    public ResponseData delete(Integer id) {
        return adminService.deleteById(id);
    }

    @Override
    public ResponseData add(Admin entity) {
        return adminService.save(entity);
    }

    @Override
    public ResponseData view(Integer id) {
        return adminService.getById(id);
    }

    /**
     *
     * @param admin 接收登录请求中的username和password参数
     * @param request 会由Springboot自动传入request对象，操作session对象
     * @param response 会由Springboot自动传入response对象，操作cookie对象
     * @return
     */
    @PostMapping("/login")
    public ResponseData login(Admin admin, HttpServletRequest request, HttpServletResponse response){
        return adminService.login(admin,request,response);
    }
}
