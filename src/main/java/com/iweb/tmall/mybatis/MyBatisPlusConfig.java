package com.iweb.tmall.mybatis;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.iweb.tmall.mapper")
public class MyBatisPlusConfig {
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        /*创建一个分页拦截器对象*/
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor(DbType.MYSQL);
        /*创建一个MyBatisPlus拦截器注册对象*/
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        /*注册分页拦截器对象*/
        mybatisPlusInterceptor.addInnerInterceptor(paginationInnerInterceptor);
        /*返回MyBatisPlus拦截器注册对象*/
        return mybatisPlusInterceptor;
    }
}
