package com.iweb.tmall.interceptor;

import com.iweb.tmall.global.CustomException;
import com.iweb.tmall.global.ErrorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Api 接口身份拦截器
 * 身份校验失败：返回失败的报文，然后在ajax中进行判断后跳转到登录页面
 */
public class ApiAuthInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String remoteToken = request.getHeader("Auth");
        //HttpSession session = request.getSession();
        //String localAuth = (String)session.getAttribute("Auth");
        //通过key 获取 redis 中存储的用户token令牌字符串，key = token-id

        if(remoteToken!=null){
            String redisToken = stringRedisTemplate.opsForValue().get("token-");
            if (redisToken != null && redisToken.equals(remoteToken)){
                return true;// 客户端身份令牌和服务端身份令牌比对成功,放行这次请求
            }
        }
        // 没有走return true,那么就抛出一个身份令牌失效的异常,让这次请求走全局异常处理器
        throw new CustomException(ErrorEnum.USER_IDENTITY_VERIFICATION_FAILS);
    }
}
