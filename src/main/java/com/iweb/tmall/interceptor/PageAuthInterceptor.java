package com.iweb.tmall.interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Page 页面身份拦截器
 * 用户请求页面，并不是由ajax发起的http请求，而是由浏览器发起的，没法操作报文参数和报文头
 * 身份令牌只能从cookie中拿，因为只有cookie是每次请求，浏览器会自动携带到请求报文中的
 * 因为只有cookie是浏览器自己请求或我们使用ajax发请求的时候都会自动携带的内容
 * 身份校验失败，直接跳转到登录页面
 */
public class PageAuthInterceptor implements HandlerInterceptor {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0){
            for (Cookie cookie : cookies){
                if ("Auth".equals(cookie.getName())){
                    //获取到浏览器携带的name为Auth的cookie的值
                    String remoteToken = cookie.getValue();
                    String redisToken = stringRedisTemplate.opsForValue().get(remoteToken);
                    if (remoteToken.endsWith(redisToken)){
                        return true;
                    }
                }
            }
        }
        //通知浏览器重定向请求到登录页面
        response.sendRedirect("/login.html");
        return false;
    }
}
