package com.iweb.tmall.interceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
//@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override //重写拦截器方法
    public void addInterceptors(InterceptorRegistry registry){

        //注册用户身份拦截器对象
        registry.addInterceptor(apiAuthInterceptor())
                .addPathPatterns("/api/**")
                .excludePathPatterns("/api/admin/login");

        //注册页面拦截器对象
        registry.addInterceptor(pageAuthInterceptor())
                .addPathPatterns("/pages/**");
    }

    @Bean
    public ApiAuthInterceptor apiAuthInterceptor(){
        return new ApiAuthInterceptor();
    }
    @Bean
    public PageAuthInterceptor pageAuthInterceptor(){
        return new PageAuthInterceptor();
    }
}
