package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Praise extends BaseEntity{
    private Long userId;
    private Long spuId;
}
