package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Address extends BaseEntity{
    private Long userId;
    private String province;
    private String city;
    private String district;
    private String detail;
    private Integer remark;
    private Integer isDefault;
}
