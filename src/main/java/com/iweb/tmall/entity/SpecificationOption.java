package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class SpecificationOption extends BaseEntity{
    private String optionValue;
    private Long specificationId;
}
