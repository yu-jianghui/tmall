package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Sku extends BaseEntity {
    private String serialNo;
    private Long spuId;
    private Integer price;
    private Integer stock;
}
