package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class SkuSpecificationOptionRelation extends BaseEntity{
    private Long skuId;
    private Long specificationOptionId;
}
