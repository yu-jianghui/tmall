package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Specification extends BaseEntity{
    private String name;
    private Long categoryId;
}
