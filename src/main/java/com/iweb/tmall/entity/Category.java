package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Category extends BaseEntity{
    private String name;
}
