package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class User extends BaseEntity {
    private String username;
    private String password;
    private String realname;
    private String sex;
    private String email;
    private String mobile;
    private String idCode;
}