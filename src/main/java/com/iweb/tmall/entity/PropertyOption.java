package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class PropertyOption extends BaseEntity{
    private String optionValue;
    private Long propertyId;
}
