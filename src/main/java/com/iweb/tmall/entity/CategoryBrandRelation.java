package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class CategoryBrandRelation extends BaseEntity{
    private Long categoryId;
    private Long brandId;
}
