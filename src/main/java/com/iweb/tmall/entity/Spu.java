package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Spu extends BaseEntity{
    private String serialNo;
    private String name;
    private String description;
    private Long categoryId;
    private Long brandId;
}