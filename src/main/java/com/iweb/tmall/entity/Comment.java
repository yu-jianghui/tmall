package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Comment extends BaseEntity{
    private Long userId;
    private Long spuId;
    private String content;
    private Integer star;
    private Integer level;
    private Long parentId;
}
