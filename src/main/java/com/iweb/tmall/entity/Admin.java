package com.iweb.tmall.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class Admin extends BaseEntity{
    private String username;
    private String password;
    @TableField(exist = false)
    private String token;//管理员登录后获得的身份令牌
}
