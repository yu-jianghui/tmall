package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class SkuPropertyOptionRelation extends BaseEntity{
    private Long skuId;
    private Long propertyOptionId;
}
