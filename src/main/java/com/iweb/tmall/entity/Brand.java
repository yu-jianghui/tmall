package com.iweb.tmall.entity;

import lombok.Data;

@Data
public class Brand extends BaseEntity{
    private String name;
}
