package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends BaseMapper<User> {
    int deleteById(User user);
}
