package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Brand;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandMapper extends BaseMapper<Brand> {
    int deleteById(Brand brand);
}
