package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.PropertyOption;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyOptionMapper extends BaseMapper<PropertyOption> {
    int deleteById(PropertyOption propertyOption);
}
