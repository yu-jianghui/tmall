package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Praise;
import org.springframework.stereotype.Repository;

@Repository
public interface PraiseMapper extends BaseMapper<Praise> {
    int deleteById(Praise praise);
}
