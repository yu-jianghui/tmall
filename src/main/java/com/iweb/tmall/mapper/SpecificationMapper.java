package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Specification;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecificationMapper extends BaseMapper<Specification> {
    int deleteById(Specification specification);
}
