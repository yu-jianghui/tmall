package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Property;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyMapper extends BaseMapper<Property> {
    int deleteById(Property property);
}
