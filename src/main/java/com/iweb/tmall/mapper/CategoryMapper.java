package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Category;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryMapper extends BaseMapper<Category> {
    int deleteById(Category category);
}
