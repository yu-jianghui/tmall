package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Admin;
import org.apache.ibatis.annotations.Options;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminMapper extends BaseMapper<Admin> {
    int deleteById(Admin admin);
}
