package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.SpecificationOption;
import org.springframework.stereotype.Repository;

@Repository
public interface SpecificationOptionMapper extends BaseMapper<SpecificationOption> {
    int deleteById(SpecificationOption specificationOption);
}
