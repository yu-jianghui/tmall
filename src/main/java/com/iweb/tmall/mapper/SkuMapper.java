package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Sku;
import org.springframework.stereotype.Repository;

@Repository
public interface SkuMapper extends BaseMapper<Sku> {
    int deleteById(Sku sku);
}
