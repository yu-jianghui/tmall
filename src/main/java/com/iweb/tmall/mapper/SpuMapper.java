package com.iweb.tmall.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.iweb.tmall.entity.Spu;
import org.springframework.stereotype.Repository;

@Repository
public interface SpuMapper extends BaseMapper<Spu> {
    int deleteById(Spu spu);
}
