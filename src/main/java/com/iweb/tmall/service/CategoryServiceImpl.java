package com.iweb.tmall.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iweb.tmall.entity.Category;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.CategoryMapper;
import com.iweb.tmall.param.CategoryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{
    @Autowired
    private CategoryMapper categoryMapper;

    @Override
    public ResponseData getList(CategoryParam param) {
        ResponseData responseData = null;
        IPage<Category> page = new Page<>(param.getCurrent(), param.getSize());//创建分页器对象
        LambdaQueryWrapper<Category> qw = getLambdaQueryWrapper();
        qw.like(param.getName() != null,Category::getName,param.getName());
        qw.eq(param.getIsDelete() != null,Category::getIsDelete,param.getIsDelete());
        /*selectList只支持传入条件构造器对象，不支持分页器*/
        //List<Category> categoryList = categoryMapper.selectList(qw);
        page = categoryMapper.selectPage(page,qw);
        //responseData = new ResponseData(categoryList);
        /*此时直接把分页器对象放入报文中进行返回，分页器对象中records属性时查询到的数据集合*/
        responseData = new ResponseData(page);
        return responseData;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = categoryMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Category entity) {
        ResponseData responseData = null;
        int row = categoryMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData("操作成功");
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Category entity) {
        ResponseData responseData = null;
        int row = categoryMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }


    @Override
    public ResponseData valid(String name) {
        ResponseData responseData = null;
        LambdaQueryWrapper<Category> qw = getLambdaQueryWrapper();
        qw.eq(Category::getName,name);
        Category one = categoryMapper.selectOne(qw);
        responseData = new ResponseData(one);
        return responseData;
    }
}
