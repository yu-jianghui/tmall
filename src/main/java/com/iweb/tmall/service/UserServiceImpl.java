package com.iweb.tmall.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iweb.tmall.entity.User;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.UserMapper;
import com.iweb.tmall.param.UserParam;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.security.util.Password;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserMapper userMapper;

    @Override
    public ResponseData getList(UserParam param) {
        ResponseData responseData = null;
        IPage<User> page = new Page<>(param.getCurrent(), param.getSize());//定义分页器对象 获取页码和页面大小
        LambdaQueryWrapper<User> qw = getLambdaQueryWrapper();
        qw.like(param.getUsername() != null,User::getUsername,param.getUsername());//用户名的模糊查询
        qw.eq(param.getIsDelete() != null,User::getIsDelete,param.getIsDelete());//是否逻辑删除查询
        page = userMapper.selectPage(page,qw);
        responseData = new ResponseData(page);
        return responseData;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = userMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(User entity) {
        ResponseData responseData = null;
        if (entity.getPassword() != null){
            entity.setPassword(DigestUtils.md5Hex(entity.getPassword()));
        }
        int row = userMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData("操作成功");
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(User entity) {
        ResponseData responseData = null;
        entity.setPassword(DigestUtils.md5Hex(entity.getPassword()));
        int row = userMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }

    @Override
    public ResponseData getOldPassword(String username) {
        ResponseData responseData = null;
        LambdaQueryWrapper<User> qw = getLambdaQueryWrapper();
        qw.eq(User::getUsername,username);
        User one = userMapper.selectOne(qw);
        responseData = new ResponseData(one);
        return responseData;
    }

    @Override
    public ResponseData validUserName(String username) {
        ResponseData responseData = null;
        LambdaQueryWrapper<User> qw = getLambdaQueryWrapper();
        qw.eq(User::getUsername,username);
        User one = userMapper.selectOne(qw);
        responseData = new ResponseData(one);
        return responseData;
    }

}
