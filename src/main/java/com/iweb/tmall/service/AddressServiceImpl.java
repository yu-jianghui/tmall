package com.iweb.tmall.service;

import com.iweb.tmall.entity.Address;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.AddressMapper;
import com.iweb.tmall.param.AddressParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService{
    @Autowired
    private AddressMapper addressMapper;

    @Override
    public ResponseData getList(AddressParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = addressMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Address entity) {
        ResponseData responseData = null;
        int row = addressMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Address entity) {
        ResponseData responseData = null;
        int row = addressMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }
}
