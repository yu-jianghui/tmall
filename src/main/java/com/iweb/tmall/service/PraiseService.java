package com.iweb.tmall.service;

import com.iweb.tmall.entity.Praise;
import com.iweb.tmall.param.PraiseParam;

public interface PraiseService extends IService<PraiseParam, Praise>{
}
