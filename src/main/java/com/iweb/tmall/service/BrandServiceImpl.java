package com.iweb.tmall.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.iweb.tmall.entity.Brand;
import com.iweb.tmall.entity.Category;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.BrandMapper;
import com.iweb.tmall.param.BrandParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandServiceImpl implements BrandService{
    @Autowired
    private BrandMapper brandMapper;

    @Override
    public ResponseData getList(BrandParam param) {
        ResponseData responseData = null;
        IPage<Brand> page = new Page<>(param.getCurrent(), param.getSize());//创建分页器对象
        LambdaQueryWrapper<Brand> qw = getLambdaQueryWrapper();
        qw.like(param.getName() != null,Brand::getName,param.getName());
        qw.eq(param.getIsDelete() != null,Brand::getIsDelete,param.getIsDelete());
        page = brandMapper.selectPage(page,qw);
        responseData = new ResponseData(page);
        return responseData;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = brandMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Brand entity) {
        ResponseData responseData = null;
        int row = brandMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData("操作成功");
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Brand entity) {
        ResponseData responseData = null;
        int row = brandMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }

    @Override
    public ResponseData valid(String name) {
        ResponseData responseData = null;
        LambdaQueryWrapper<Brand> qw = getLambdaQueryWrapper();
        qw.eq(Brand::getName,name);
        Brand one = brandMapper.selectOne(qw);
        responseData = new ResponseData(one);
        return responseData;
    }
}
