package com.iweb.tmall.service;

import com.iweb.tmall.entity.Comment;
import com.iweb.tmall.param.CommentParam;

public interface CommentService extends IService<CommentParam, Comment>{
}
