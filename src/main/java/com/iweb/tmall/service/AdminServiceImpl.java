package com.iweb.tmall.service;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.iweb.tmall.entity.Admin;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.AdminMapper;
import com.iweb.tmall.param.AdminParam;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class AdminServiceImpl implements AdminService{
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public ResponseData getList(AdminParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = adminMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Admin entity) {
        ResponseData responseData = null;
        entity.setPassword(DigestUtils.md5Hex(entity.getPassword()));
        int row = adminMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Admin entity) {
        ResponseData responseData = null;
        //在调用mapper层插入管理员之前,先取出密码进行加密
        //DigestUtils.md5Hex()方法中使用了md5加密算法，参数是加密的字符串
        // entity对象就是从控制器传过来的管理员实体对象
        // 从entity中取出密码,加密后再存入entity即可
        entity.setPassword(DigestUtils.md5Hex(entity.getPassword()));
        // 这样在调用mapper层的时候, 传过去的entity对象中的密码就是加密过的
        int row = adminMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }

    @Override
    public ResponseData login(Admin admin, HttpServletRequest request, HttpServletResponse response) {
        ResponseData responseData = null;
        LambdaQueryWrapper<Admin> qw = getLambdaQueryWrapper(admin)
                .eq(Admin::getUsername,admin.getUsername());
        Admin one = adminMapper.selectOne(qw);
        if (one != null){
            if (one.getPassword().equals(DigestUtils.md5Hex(admin.getPassword()))){
                //用户存在且密码校验成功，颁发token身份令牌(随机的UUID并加密后得到的字符串)
                String token = DigestUtils.md5Hex(UUID.randomUUID().toString());
                stringRedisTemplate.opsForValue().set(token,token,10, TimeUnit.SECONDS);
                //token缓存在本地session中
                //HttpSession session = request.getSession();
                //session.setAttribute("Auth",token);
                //token缓存到cookie中返回给客户端浏览器
                Cookie cookie = new Cookie("Auth",token);
                cookie.setPath("/");
                cookie.setMaxAge(60*30);
                response.addCookie(cookie);
                //token做完报文一起返回给ajax,把token存到admin对象中，把admin对象存到ResponseData对象中进行返回
                one.setToken(token);
                responseData = new ResponseData(one);
            }else {
                responseData = new ResponseData(ErrorEnum.PASSWORD_VERIFICATION_FAILD);
            }
        }else {
            responseData = new ResponseData(ErrorEnum.ACCOUNT_VERIFICATION_FAILD);
        }
        return responseData;
    }
}
