package com.iweb.tmall.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.iweb.tmall.entity.BaseEntity;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.BaseParam;

public interface IService <P extends BaseParam,E extends BaseEntity>{
    ResponseData getList(P param);
    @Deprecated
    ResponseData deleteById(Integer id);
    ResponseData updateById(E entity);
    ResponseData save(E entity);
    ResponseData getById(Integer id);
    default QueryWrapper<E> getQueryWrapper(){
        return new QueryWrapper<E>().eq("is_delete",0);
    }
    default LambdaQueryWrapper<E> getLambdaQueryWrapper(E entity){
        return new LambdaQueryWrapper<E>()
                .setEntityClass((Class<E>) entity.getClass())
                .eq(E::getIsDelete,0);
    }
    default LambdaQueryWrapper<E> getLambdaQueryWrapper(){
        return new LambdaQueryWrapper<E>();
    }
}
