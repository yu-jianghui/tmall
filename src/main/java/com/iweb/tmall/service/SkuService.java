package com.iweb.tmall.service;

import com.iweb.tmall.entity.Sku;
import com.iweb.tmall.param.SkuParam;

public interface SkuService extends IService<SkuParam, Sku>{
}
