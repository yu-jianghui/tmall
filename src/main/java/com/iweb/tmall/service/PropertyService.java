package com.iweb.tmall.service;

import com.iweb.tmall.entity.Property;
import com.iweb.tmall.param.PropertyParam;

public interface PropertyService extends IService<PropertyParam, Property>{
}
