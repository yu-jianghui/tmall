package com.iweb.tmall.service;

import com.iweb.tmall.entity.User;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.UserParam;

public interface UserService extends IService<UserParam, User>{
    ResponseData getOldPassword(String username);

    ResponseData validUserName(String username);
}
