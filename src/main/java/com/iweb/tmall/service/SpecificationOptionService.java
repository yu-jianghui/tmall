package com.iweb.tmall.service;

import com.iweb.tmall.entity.SpecificationOption;
import com.iweb.tmall.param.SpecificationOptionParam;

public interface SpecificationOptionService extends IService<SpecificationOptionParam, SpecificationOption>{
}
