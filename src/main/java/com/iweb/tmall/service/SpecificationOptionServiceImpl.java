package com.iweb.tmall.service;

import com.iweb.tmall.entity.SpecificationOption;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.SpecificationOptionMapper;
import com.iweb.tmall.param.SpecificationOptionParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpecificationOptionServiceImpl implements SpecificationOptionService{
    @Autowired
    private SpecificationOptionMapper specificationOptionMapper;

    @Override
    public ResponseData getList(SpecificationOptionParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = specificationOptionMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(SpecificationOption entity) {
        ResponseData responseData = null;
        int row = specificationOptionMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(SpecificationOption entity) {
        ResponseData responseData = null;
        int row = specificationOptionMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }
}
