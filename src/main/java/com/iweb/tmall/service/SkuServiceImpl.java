package com.iweb.tmall.service;

import com.iweb.tmall.entity.Sku;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.SkuMapper;
import com.iweb.tmall.param.SkuParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SkuServiceImpl implements SkuService{
    @Autowired
    private SkuMapper skuMapper;

    @Override
    public ResponseData getList(SkuParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = skuMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Sku entity) {
        ResponseData responseData = null;
        int row = skuMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Sku entity) {
        ResponseData responseData = null;
        int row = skuMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }
}
