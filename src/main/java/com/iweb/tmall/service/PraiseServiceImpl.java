package com.iweb.tmall.service;

import com.iweb.tmall.entity.Praise;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.PraiseMapper;
import com.iweb.tmall.param.PraiseParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PraiseServiceImpl implements PraiseService{
    @Autowired
    private PraiseMapper praiseMapper;

    @Override
    public ResponseData getList(PraiseParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = praiseMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(Praise entity) {
        ResponseData responseData = null;
        int row = praiseMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(Praise entity) {
        ResponseData responseData = null;
        int row = praiseMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }
}
