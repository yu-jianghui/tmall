package com.iweb.tmall.service;

import com.iweb.tmall.entity.Admin;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.AdminParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AdminService extends IService<AdminParam, Admin>{

    ResponseData login(Admin admin, HttpServletRequest request, HttpServletResponse response);

}
