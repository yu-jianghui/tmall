package com.iweb.tmall.service;

import com.iweb.tmall.entity.Address;
import com.iweb.tmall.param.AddressParam;

public interface AddressService extends IService<AddressParam, Address>{
}
