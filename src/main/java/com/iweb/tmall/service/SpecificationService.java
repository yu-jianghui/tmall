package com.iweb.tmall.service;

import com.iweb.tmall.entity.Specification;
import com.iweb.tmall.param.SpecificationParam;

public interface SpecificationService extends IService<SpecificationParam, Specification>{
}
