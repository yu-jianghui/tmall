package com.iweb.tmall.service;

import com.iweb.tmall.entity.PropertyOption;
import com.iweb.tmall.param.PropertyOptionParam;

public interface PropertyOptionService extends IService<PropertyOptionParam, PropertyOption>{
}
