package com.iweb.tmall.service;

import com.iweb.tmall.entity.Spu;
import com.iweb.tmall.param.SpuParam;

public interface SpuService extends IService<SpuParam, Spu>{
}
