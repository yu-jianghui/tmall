package com.iweb.tmall.service;

import com.iweb.tmall.entity.PropertyOption;
import com.iweb.tmall.global.ErrorEnum;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.mapper.PropertyOptionMapper;
import com.iweb.tmall.param.PropertyOptionParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PropertyOptionServiceImpl implements PropertyOptionService{
    @Autowired
    private PropertyOptionMapper propertyOptionMapper;
    @Override
    public ResponseData getList(PropertyOptionParam param) {
        return null;
    }

    @Override
    public ResponseData deleteById(Integer id) {
        ResponseData responseData = null;
        int row = propertyOptionMapper.deleteById(id);
        if (row > 0){
            responseData = new ResponseData(id);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData updateById(PropertyOption entity) {
        ResponseData responseData = null;
        int row = propertyOptionMapper.updateById(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData save(PropertyOption entity) {
        ResponseData responseData = null;
        int row = propertyOptionMapper.insert(entity);
        if (row > 0){
            responseData = new ResponseData(entity);
        }else {
            responseData = new ResponseData(ErrorEnum.DATABASE_SERVICE_ERROR);
        }
        return responseData;
    }

    @Override
    public ResponseData getById(Integer id) {
        return null;
    }
}
