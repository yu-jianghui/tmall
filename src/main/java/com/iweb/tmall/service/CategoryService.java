package com.iweb.tmall.service;

import com.iweb.tmall.entity.Category;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.CategoryParam;

public interface CategoryService extends IService<CategoryParam, Category>{
    ResponseData valid(String name);
}
