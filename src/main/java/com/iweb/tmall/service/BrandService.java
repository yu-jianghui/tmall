package com.iweb.tmall.service;

import com.iweb.tmall.entity.Brand;
import com.iweb.tmall.global.ResponseData;
import com.iweb.tmall.param.BrandParam;

public interface BrandService extends IService<BrandParam, Brand>{
    ResponseData valid(String name);
}
