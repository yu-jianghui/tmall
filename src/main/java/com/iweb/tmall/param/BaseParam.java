package com.iweb.tmall.param;

import lombok.Data;

@Data
public class BaseParam {
    private Integer current;
    private Integer size;
    private Integer isDelete;
}
