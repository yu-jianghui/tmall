package com.iweb.tmall.param;

import lombok.Data;

@Data
public class BrandParam extends BaseParam{
    private String name;
}
