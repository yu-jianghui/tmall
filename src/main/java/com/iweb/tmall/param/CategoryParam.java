package com.iweb.tmall.param;

import lombok.Data;

@Data
public class CategoryParam extends BaseParam{
    private String name;
}
