package com.iweb.tmall.param;

import lombok.Data;

@Data
public class UserParam extends BaseParam{
    private String username;
}
