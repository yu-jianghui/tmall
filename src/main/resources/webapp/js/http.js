/* 封装http对象, 用来封装ajax请求*/
var http = {
    BASE_URL:'http://localhost:8080/', // 基础URL地址
    serviceError:function(response){ /* 业务失败的处理方法 */
        if(response.code == "A0220"){
            // 只有token校验失败这个错误需要在3秒后跳转到登录页面
            lightyear.notify('token校验失败,3秒后跳转到登录页面', 'danger', 3000, 'mdi mdi-emoticon-happy', 'top', 'center' ,this.BASE_URL + "login.html");
        }else{
            // 其他的错误会直接显示错误码和错误信息即可, 不跳页面
            lightyear.notify(`错误码:${response.code}<br/>错误信息:${response.message}`, 'danger', 3000, '', 'top', 'center');
        }
    },
    httpError:function(code,message){ /* http请求失败的处理方法 */
        if(code == 404){ // 如果http状态码是404, 就跳转到404页面
            location.href = this.BASE_URL + "404.html";
        }else{          // 其他所有错误的状态码, 都跳转到500页面
            location.href = this.BASE_URL + "500.html";
        }
    },
    get:function (url,data,successFun) {
        $.ajax({
            url:this.BASE_URL + url,
            method:'GET',
            data:data,
            dataType:'json',
            beforeSend:(request) =>{ // 发送ajax之前从浏览器本地存储对象中取出身份令牌, 存到请求头里面
                try {
                    var token = JSON.parse(localStorage.getItem("admin")).token;
                    request.setRequestHeader("Auth",token);
                }catch (e) {
                    console.log(e);
                }
            },
            success: (response) => { // http状态码200, 响应报文能够成功返回
                if(response.code == "00000"){ // 结果码是"00000"表示业务成功
                    // 结果码是"00000" 说明请求成功, 直接把response.data 回调给success函数
                    successFun(response.data);
                }else{
                    // 统一业务失败处理
                    this.serviceError(response);
                }
            },
            error:(code,message) => { // http状态码不是200,可能是404,403,405,500等等
                // 统一http请求失败处理
                this.httpError(code,message);
            }
        })
    },
    post:function (url,data,successFun) {
        $.ajax({
            url:this.BASE_URL + url,
            method:'POST',
            data:data,
            dataType:'json',
            beforeSend:(request) =>{ // 发送ajax之前从浏览器本地存储对象中取出身份令牌, 存到请求头里面
                try {
                    var token = JSON.parse(localStorage.getItem("admin")).token;
                    request.setRequestHeader("Auth",token);
                }catch (e) {
                    console.log(e);
                }
            },
            success:(response) =>{ // http状态码200, 响应报文能够成功返回
                if(response.code == "00000"){
                    // 结果码是"00000" 说明请求成功, 直接把response.data 回调给success函数
                    successFun(response.data);
                }else{
                    // 统一业务失败处理
                    this.serviceError(response);
                }
            },
            error: (code,message) => {// http状态码不是200,可能是404,403,405,500等等
                // 统一http请求失败处理
                this.httpError(code,message);
            }
        })
    }
}